Embedding Keycloak Server in a Spring-Boot App 
----------------------------------------------------------

To start the sample application just build the project with: 
```
mvn package
```

and start the spring boot app via:
```
java -jar target/spring-boot-keycloak-server-*.jar
```

The embedded Keycloak server is now reachable via http://localhost:8080/auth.




